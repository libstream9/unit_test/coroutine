#include <stream9/coroutine/read.hpp>

#include "namespace.hpp"

#include <stream9/string.hpp>
#include <stream9/string_view.hpp>
#include <stream9/coroutine/event_scheduler.hpp>

#include <boost/test/unit_test.hpp>

#include <fcntl.h>
#include <unistd.h>

namespace testing {

using st9::string_view;
using st9::string;

BOOST_AUTO_TEST_SUITE(read_)

    co::task<void> array_sender(int fd, string_view msg)
    {
        auto n = ::write(fd, msg.data(), msg.size());
        BOOST_REQUIRE(n != -1);

        co_return;
    }

    co::task<string> array_receiver(int fd)
    {
        try {
            char buf[256];
            auto n = (co_await co::read(fd, buf)).or_throw();

            co_return string_view { buf, n };
        }
        catch (...) {
            st9::rethrow_error();
        }
    }

    BOOST_AUTO_TEST_CASE(array_)
    {
        int pfd[2];
        auto rc = ::pipe2(pfd, O_NONBLOCK);
        BOOST_REQUIRE(rc != -1);

        co::event_scheduler sched;

        auto msg = "hello";

        auto t1 = array_sender(pfd[1], msg);
        auto t2 = array_receiver(pfd[0]);

        sched.schedule(t2);
        sched.schedule(t1);

        sched.run();

        BOOST_TEST(t2.result() == msg);
    }

    co::task<void> int_sender(int fd, int msg)
    {
        auto n = ::write(fd, &msg, sizeof(msg));
        BOOST_REQUIRE(n == sizeof(msg));

        co_return;
    }

    co::task<int> int_receiver(int fd)
    {
        try {
            int msg;
            auto n = (co_await co::read(fd, msg)).or_throw();
            BOOST_REQUIRE(n == sizeof(msg));

            co_return msg;
        }
        catch (...) {
            st9::rethrow_error();
        }
    }

    BOOST_AUTO_TEST_CASE(int_)
    {
        int pfd[2];
        auto rc = ::pipe2(pfd, O_NONBLOCK);
        BOOST_REQUIRE(rc != -1);

        co::event_scheduler sched;

        auto msg = 1;

        auto t1 = int_sender(pfd[1], msg);
        auto t2 = int_receiver(pfd[0]);

        sched.schedule(t2);
        sched.schedule(t1);

        sched.run();

        BOOST_TEST(t2.result() == msg);
    }

BOOST_AUTO_TEST_SUITE_END() // read_

} // namespace testing
