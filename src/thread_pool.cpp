#include <stream9/coroutine/thread_pool.hpp>

#include "namespace.hpp"

#include <syncstream>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(thread_pool_)

    BOOST_AUTO_TEST_CASE(default_constructor_)
    {
        co::thread_pool p;

        BOOST_TEST(p.thread_count() == std::thread::hardware_concurrency());
    }

    BOOST_AUTO_TEST_CASE(specify_size_)
    {
        co::thread_pool p { 2 };

        BOOST_TEST(p.thread_count() == 2);
    }

    BOOST_AUTO_TEST_CASE(wait_on_empty_)
    {
        co::thread_pool p { 2 };

        p.wait();
    }

    BOOST_AUTO_TEST_CASE(execute_and_wait_)
    {
        co::thread_pool p { 2 };

        auto work = [](int i) {
            return [i] {
                std::this_thread::sleep_for(100ms);
            };
        };

        p.execute(work(1));
        p.execute(work(2));
        p.execute(work(3));
        p.execute(work(4));

        BOOST_TEST(p.work_count() > 0);

        p.wait();
    }

BOOST_AUTO_TEST_SUITE_END() // thread_pool_

} // namespace testing
