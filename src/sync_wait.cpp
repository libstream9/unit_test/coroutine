#include <stream9/coroutine/sync_wait.hpp>

#include <stream9/coroutine/task.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace co = stream9::coroutine;

BOOST_AUTO_TEST_SUITE(sync_wait_)

    co::task<> task1() { co_return; }

    BOOST_AUTO_TEST_CASE(step1_)
    {
        auto t = task1();

        co::sync_wait(t);
    }

    co::task<int> task2() { co_return 1; }

    BOOST_AUTO_TEST_CASE(step2_)
    {
        auto t = task2();

        auto r = co::sync_wait(t);

        BOOST_TEST(r == 1);
        BOOST_TEST(t.get() == 1);
    }

    BOOST_AUTO_TEST_CASE(step3_)
    {
        using namespace std::literals;

        auto t = task2();

        auto r = co::sync_wait_for(t, 1s);
        BOOST_CHECK(r == std::cv_status::no_timeout);

        BOOST_TEST(t.get() == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // sync_wait_

} // namespace testing
