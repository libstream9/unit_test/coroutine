#include <stream9/coroutine/semaphore.hpp>

#include "namespace.hpp"

#include <future>
#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(counting_semaphore_)

    BOOST_AUTO_TEST_CASE(construction_)
    {
        co::counting_semaphore<> s { 0 };
    }

    BOOST_AUTO_TEST_CASE(try_acquire_)
    {
        co::counting_semaphore<> s { 1 };

        BOOST_TEST(s.try_acquire());
        BOOST_TEST(!s.try_acquire());
    }

    BOOST_AUTO_TEST_CASE(release_)
    {
        co::counting_semaphore<> s { 0 };

        BOOST_TEST(!s.try_acquire());

        s.release(2);
        BOOST_TEST(s.try_acquire());
        BOOST_TEST(s.try_acquire());
        BOOST_TEST(!s.try_acquire());
    }

    BOOST_AUTO_TEST_CASE(acquire_)
    {
        co::binary_semaphore s { 0 };

        auto t = s.acquire();
        t.resume();

        s.release();
    }

    BOOST_AUTO_TEST_CASE(acquire_thread_)
    {
        co::binary_semaphore s { 0 };

#if 0
        std::jthread t1 { [&] {
            auto t = s.acquire();
            t.resume();
        } };
#endif
        auto f1 = std::async([&] {
            auto t = s.acquire();
            t.resume();
            return t;
        });

        std::this_thread::sleep_for(10ms);
        s.release();
    }

BOOST_AUTO_TEST_SUITE_END() // counting_semaphore_

} // namespace testing
