#include <stream9/coroutine/transform.hpp>

#include "namespace.hpp"

#include <stream9/coroutine/task.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(transform_)

    co::task<int> coro_int(int i)
    {
        co_return i * 2;
    }

    BOOST_AUTO_TEST_CASE(step1_)
    {
        auto t1 = coro_int(1);
        auto t2 = co::transform(t1, [](auto i) { return i * 2; });

        auto r = co::sync_wait(t2);

        BOOST_TEST(r == 4);
    }

    BOOST_AUTO_TEST_CASE(pipe_)
    {
        auto t = coro_int(1)
               | co::transform([](auto i) { return i * 2; });

        auto r = co::sync_wait(t);

        BOOST_TEST(r == 4);
    }

BOOST_AUTO_TEST_SUITE_END() // transform_

} // namespace testing
