#include <stream9/coroutine/type_traits.hpp>

#include <stream9/coroutine/task.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace co = stream9::coroutine;

BOOST_AUTO_TEST_SUITE(type_traits_)

    co::task<> coro1() { co_return; }
    co::task<int> coro2() { co_return 1; }

    struct func1 {
        co::task<int> coro3() { co_return 1; }
    };

    BOOST_AUTO_TEST_CASE(is_coroutine_)
    {
        static_assert(co::is_coroutine_v(coro1));
        static_assert(co::is_coroutine_v(coro2));

        static_assert(co::is_coroutine_v(&func1::coro3));

        auto coro4 = []() -> co::task<> { co_return; };
        static_assert(co::is_coroutine_v(coro4));
    }

    struct task_1 {};
    struct task_1_awaiter {};
    auto operator co_await(task_1 const&) { return task_1_awaiter(); }

    BOOST_AUTO_TEST_CASE(has_adl_co_await_)
    {
        static_assert(co::has_adl_co_await<task_1>);
    }

    struct task_2 {
        auto operator co_await() { return 0; };
    };

    BOOST_AUTO_TEST_CASE(has_member_co_await_)
    {
        static_assert(co::has_member_co_await<task_2>);
    }

BOOST_AUTO_TEST_SUITE_END() // type_traits_

} // namespace testing
