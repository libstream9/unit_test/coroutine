#include <stream9/coroutine/when_all.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(wait_all_)

    co::task<> coro_void(int = 0)
    {
        co_return;
    }

    co::task<int> coro_int(int i)
    {
        co_return i;
    }

    BOOST_AUTO_TEST_CASE(one_void_)
    {
        auto t1 = coro_void();

        auto tx = co::when_all(t1);

        static_assert(std::same_as<decltype(tx), co::task<>>);

        tx.wait();
    }

    BOOST_AUTO_TEST_CASE(one_int_)
    {
        auto t1 = coro_int(1);

        auto tx = co::when_all(t1);

        static_assert(std::same_as<decltype(tx), co::task<int>>);

        auto r = co::sync_wait(tx);

        BOOST_TEST(r == 1);
    }

    BOOST_AUTO_TEST_CASE(all_void_)
    {
        auto t1 = coro_void();
        auto t2 = coro_void();
        auto t3 = coro_void();

        static_assert(std::same_as<decltype(t1), co::task<>>);

        auto tx = co::when_all(t1, t2, t3);

        tx.wait();
    }

    BOOST_AUTO_TEST_CASE(all_int_)
    {
        auto t1 = coro_int(1);
        auto t2 = coro_int(2);
        auto t3 = coro_int(3);

        auto tx = co::when_all(t1, t2, t3);

        static_assert(std::same_as<decltype(tx), co::task<std::tuple<int, int, int>>>);

        auto r = co::sync_wait(tx);

        BOOST_TEST(std::tuple_size_v<decltype(r)> == 3);
        BOOST_TEST(std::get<0>(r) == 1);
        BOOST_TEST(std::get<1>(r) == 2);
        BOOST_TEST(std::get<2>(r) == 3);
    }

    BOOST_AUTO_TEST_CASE(mix_type_1_)
    {
        auto t1 = coro_int(1);
        auto t2 = coro_void(2);
        auto t3 = coro_int(3);

        auto tx = co::when_all(t1, t2, t3);

        auto r = co::sync_wait(tx);

        BOOST_TEST(std::tuple_size_v<decltype(r)> == 2);
        BOOST_TEST(std::get<0>(r) == 1);
        BOOST_TEST(std::get<1>(r) == 3);
    }

    BOOST_AUTO_TEST_CASE(mix_type_2_)
    {
        auto t1 = coro_int(1);
        auto t2 = coro_int(2);
        auto t3 = coro_void(3);

        auto tx = co::when_all(t1, t2, t3);

        auto r = co::sync_wait(tx);

        BOOST_TEST(std::tuple_size_v<decltype(r)> == 2);
        BOOST_TEST(std::get<0>(r) == 1);
        BOOST_TEST(std::get<1>(r) == 2);
    }

    BOOST_AUTO_TEST_CASE(mix_type_3_)
    {
        auto coro_tuple = [](int i) -> co::task<std::tuple<int, int>> {
            co_return std::make_tuple(i, i * 2);
        };

        auto t1 = coro_int(1);
        auto t2 = coro_tuple(2);
        auto t3 = coro_int(3);

        auto tx = co::when_all(t1, t2, t3);

        auto r = co::sync_wait(tx);
        static_assert(std::same_as<decltype(r),
                      std::tuple<int, std::tuple<int, int>, int> >);
    }

BOOST_AUTO_TEST_SUITE_END() // wait_all_

} // namespace testing

