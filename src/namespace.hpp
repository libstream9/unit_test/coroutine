#ifndef STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP
#define STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP

namespace std::ranges {}

namespace stream9::coroutine {}

namespace testing {

namespace co { using namespace stream9::coroutine; }
namespace rng { using namespace std::ranges; }

} // namespace testing

#endif // STREAM9_SDBUS_TEST_SRC_NAMESPACE_HPP
