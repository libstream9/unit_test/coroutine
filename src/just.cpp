#include <stream9/coroutine/just.hpp>

#include "namespace.hpp"

#include <stream9/coroutine/sync_wait.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(just_)

    BOOST_AUTO_TEST_CASE(step1_)
    {
        auto r = co::sync_wait(co::just(1));

        BOOST_TEST(r == 1);
    }

BOOST_AUTO_TEST_SUITE_END() // just_

} // namespace testing
