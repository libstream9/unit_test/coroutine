#include <stream9/coroutine/generator.hpp>

#include <algorithm>
#include <concepts>
#include <iterator>
#include <stdexcept>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace rng { using namespace std::ranges; }

namespace coro = stream9::coroutine;

BOOST_AUTO_TEST_SUITE(generator_)

    BOOST_AUTO_TEST_SUITE(value_type_)

        coro::generator<int> gen()
        {
            int i = 300;
            co_yield 100;
            co_yield 200;
            co_yield i;
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<int> result;

            using T = decltype(gen());

            static_assert(rng::input_range<T>);
            static_assert(std::same_as<T::value_type, int>);
            static_assert(std::same_as<rng::range_value_t<T>, int>);
            static_assert(std::same_as<rng::range_reference_t<T>, int&>);
            static_assert(std::same_as<rng::range_rvalue_reference_t<T>, int&&>);

            rng::copy(gen(), std::back_inserter(result));

            int expected[] = { 100, 200, 300, };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // value_type_

    BOOST_AUTO_TEST_SUITE(lvalue_reference_)

        coro::generator<int&> gen()
        {
            static int v[] = { 10, 20, 30, 40, };

            for (auto& i: v) {
                co_yield i;
            }

            //co_yield 50; // this should be compile error
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            using T = decltype(gen());

            static_assert(rng::input_range<T>);
            static_assert(std::same_as<T::value_type, int&>);
            static_assert(std::same_as<rng::range_value_t<T>, int>);
            static_assert(std::same_as<rng::range_reference_t<T>, int&>);
            static_assert(std::same_as<rng::range_rvalue_reference_t<T>, int&&>);

            std::vector<int> result;

            rng::copy(gen(), std::back_inserter(result));

            int expected[] = { 10, 20, 30, 40 };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // lvalue_reference_

    BOOST_AUTO_TEST_SUITE(const_lvalue_reference_)

        coro::generator<int const&> gen()
        {
            static int v[] = { 10, 20, 30, };

            for (auto& i: v) {
                co_yield i;
            }

            co_yield 40;
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            using T = decltype(gen());

            static_assert(rng::input_range<T>);
            static_assert(std::same_as<T::value_type, int const&>);
            static_assert(std::same_as<rng::range_value_t<T>, int>); // std::indirectly_readable_traits remove const
            static_assert(std::same_as<rng::range_reference_t<T>, int const&>);
            static_assert(std::same_as<rng::range_rvalue_reference_t<T>, int const&&>);

            std::vector<int> result;

            rng::copy(gen(), std::back_inserter(result));

            int expected[] = { 10, 20, 30, 40 };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // const_lvalue_reference_

    BOOST_AUTO_TEST_SUITE(move_only_value_)

        class value
        {
        public:
            value(int i) : m_value { i } {}

            value(value const&) = delete;
            value& operator=(value const&) = delete;

            value(value&&) = default;
            value& operator=(value&&) = default;

            int operator*() const { return m_value; }

            bool operator==(value const&) const = default;

            friend std::ostream& operator<<(std::ostream& os, value const& v)
            {
                return os << v.m_value;
            }

        private:
            int m_value = 0;
        };

        coro::generator<value> gen()
        {
            co_yield value { 100 };

            value v2 { 200 }; co_yield std::move(v2);
            //value v1 { 300 }; co_yield v1; //should be error
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<value> result;

            using T = decltype(gen());
            static_assert(std::same_as<T::iterator::reference, value&>);

            for (auto& v: gen()) {
                result.push_back(std::move(v));
            }

            value expected[] = { 100, 200, };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // move_only_

    BOOST_AUTO_TEST_SUITE(copy_only_value_)

        class value
        {
        public:
            value(int i) : m_value { i } {}

            value(value const&) = default;
            value& operator=(value const&) = default;

            value(value&&) = delete;
            value& operator=(value&&) = delete;

            int operator*() const { return m_value; }

            bool operator==(value const&) const = default;

            friend std::ostream& operator<<(std::ostream& os, value const& v)
            {
                return os << v.m_value;
            }

        private:
            int m_value = 0;
        };

        coro::generator<value> gen()
        {
            co_yield value { 100 };

            value v1 { 200 }; co_yield std::move(v1);

            value v2 { 300 }; co_yield v2;

            //value v3 { 400 }; co_yield v3; //should be error
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<value> result;

            rng::copy(gen(), std::back_inserter(result));

            value expected[] = { 100, 200, 300, };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // copy_only_

    BOOST_AUTO_TEST_SUITE(exception_)

        coro::generator<int> gen()
        {
            co_yield 1;
            co_yield 2;
            throw std::runtime_error("foo");
            co_yield 3;
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<int> result;

            BOOST_CHECK_THROW(
                rng::copy(gen(), std::back_inserter(result)),
                std::runtime_error
            );

            int expected[] = { 1, 2, };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // exception_

    BOOST_AUTO_TEST_SUITE(empty_)

        coro::generator<int> gen()
        {
            co_return;
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<int> result;

            rng::copy(gen(), std::back_inserter(result));

            BOOST_TEST(result.empty());

            auto g = gen();
            BOOST_TEST(g.empty());
        }

    BOOST_AUTO_TEST_SUITE_END() // empty_

    BOOST_AUTO_TEST_SUITE(not_empty_)

        coro::generator<int> gen()
        {
            co_yield 1;
        }

        BOOST_AUTO_TEST_CASE(test_)
        {
            std::vector<int> result;

            auto g = gen();
            BOOST_TEST(!g.empty());

            rng::copy(g, std::back_inserter(result));

            int const expected[] = { 1 };

            BOOST_TEST(result == expected, boost::test_tools::per_element());
        }

    BOOST_AUTO_TEST_SUITE_END() // empty_

BOOST_AUTO_TEST_SUITE_END() // generator_

} // namespace testing
