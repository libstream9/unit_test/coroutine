#include <stream9/coroutine/barrier.hpp>

#include "namespace.hpp"

#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(barrier_)

    BOOST_AUTO_TEST_CASE(construction_)
    {
        co::barrier b { 1 };
    }

    BOOST_AUTO_TEST_CASE(arrive_)
    {
        co::barrier b { 1 };

        auto t = b.wait();
        t.resume();
        BOOST_TEST(!t.done());

        b.arrive();
        BOOST_TEST(t.done());
    }

    BOOST_AUTO_TEST_CASE(arrive_thread_)
    {
        co::barrier b { 1 };

        std::jthread t1 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        auto co_main = [&]() -> co::task<> {
            //co_await b.wait(); //GCCBUG #99576
            auto t = b.wait(); co_await t;
        };

        co_main().wait();
    }

    BOOST_AUTO_TEST_CASE(completion_)
    {
        bool completion = false;
        co::barrier b { 1, [&]() {
            completion = true;
        } };

        std::jthread t1 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        auto co_main = [&]() -> co::task<> {
            //co_await b.wait(); //GCCBUG #99576
            auto t = b.wait(); co_await t;
        };

        co_main().wait();
        t1.join();

        BOOST_TEST(completion);
    }

    BOOST_AUTO_TEST_CASE(multiple_phase_)
    {
        co::barrier b { 1 };

        auto co_main = [&]() -> co::task<> {
            //co_await b.wait(); //GCCBUG #99576
            auto t = b.wait(); co_await t;
        };

        // phase 1
        std::jthread t1 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        co_main().wait();
        t1.join();

        // phase 2
        std::jthread t2 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        co_main().wait();
        t2.join();
    }

    BOOST_AUTO_TEST_CASE(arrive_and_drop_)
    {
        co::barrier b { 2 };

        auto co_main = [&]() -> co::task<> {
            //co_await b.wait(); //GCCBUG #99576
            auto t = b.wait(); co_await t;
        };

        // phase 1
        std::jthread t1 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        std::jthread t2 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive_and_drop();
        } };

        co_main().wait();
        t1.join(); t2.join();

        // phase 2
        std::jthread t3 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        co_main().wait();
        t3.join();
    }

    BOOST_AUTO_TEST_CASE(arrive_and_wait_)
    {
        co::barrier b { 2 };

        auto co_main = [&]() -> co::task<> {
            //co_await b.arrive_and_wait(); //GCCBUG #99576
            auto t = b.arrive_and_wait(); co_await t;
        };

        // phase 1
        std::jthread t1 { [&] {
            std::this_thread::sleep_for(100ms);
            b.arrive();
        } };

        co_main().wait();
        t1.join();
    }

BOOST_AUTO_TEST_SUITE_END() // barrier_

} // namespace testing
