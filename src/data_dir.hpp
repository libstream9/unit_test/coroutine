#ifndef TEST_DATA_DIR_HPP
#define TEST_DATA_DIR_HPP

#include <filesystem>

#include <boost/preprocessor/stringize.hpp>

namespace testing {

inline std::filesystem::path
data_dir()
{
    return fs::path(BOOST_PP_STRINGIZE(DATA_DIR));
}

} // namespace testing

#endif // TEST_DATA_DIR_HPP
