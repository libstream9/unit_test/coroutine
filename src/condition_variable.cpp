#include <stream9/coroutine/condition_variable.hpp>

#include <future>
#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace co = stream9::coroutine;

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(condition_variable_)

    BOOST_AUTO_TEST_CASE(notify_one_)
    {
        std::mutex m;
        co::condition_variable cv;

        std::unique_lock lk { m };
        auto task = cv.wait(lk);
        task.resume();

        cv.notify_one();
    }

    BOOST_AUTO_TEST_CASE(notify_one_thread_)
    {
        std::mutex m;
        co::condition_variable cv;

        auto coro_wait = [&]() -> co::task<> {
            std::unique_lock lk { m };
            co_await cv.wait(lk);
        };

        std::jthread t1 {
            [&] { coro_wait().wait(); }
        };

        std::this_thread::sleep_for(100ms);
        cv.notify_one();
    }

    BOOST_AUTO_TEST_CASE(notify_all_)
    {
        std::mutex m;
        co::condition_variable cv;

        std::unique_lock lk { m };
        auto t1 = cv.wait(lk);
        t1.resume();

        auto t2 = cv.wait(lk);
        t2.resume();

        auto t3 = cv.wait(lk);
        t3.resume();

        cv.notify_all();
    }

    BOOST_AUTO_TEST_CASE(notify_all_thread_)
    {
        std::mutex m;
        co::condition_variable cv;

        auto coro_wait = [&]() -> co::task<> {
            std::unique_lock lk { m };
            co_await cv.wait(lk);
        };

        auto start_wait_thread = [&] {
            return std::jthread {
                [&] { coro_wait().wait(); }
            };
        };

        auto t1 = start_wait_thread();
        auto t2 = start_wait_thread();
        auto t3 = start_wait_thread();

        std::this_thread::sleep_for(100ms);
        cv.notify_all();
    }

BOOST_AUTO_TEST_SUITE_END() // condition_variable_

} // namespace testing
