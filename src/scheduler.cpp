#include <stream9/coroutine/basic_scheduler.hpp>

#include "namespace.hpp"

#include <stream9/coroutine/latch.hpp>
#include <stream9/coroutine/thread_pool.hpp>

#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(basic_scheduler_)

    BOOST_AUTO_TEST_CASE(step1_)
    {
        co::basic_scheduler e;

        auto coro1 = []() -> co::task<> {
            co_return;
        };

        e.schedule(coro1());

        e.run();
    }

    BOOST_AUTO_TEST_CASE(with_thread_pool_1_)
    {
        co::thread_pool p { 2 };
        co::basic_scheduler s { p };

        auto coro1 = []() -> co::task<> {
            co_return;
        };

        s.schedule(coro1());
        s.schedule(coro1());
        s.schedule(coro1());

        s.run();
        p.wait();
    }

    BOOST_AUTO_TEST_CASE(with_thread_pool_2_)
    {
        co::thread_pool p { 2 };
        co::basic_scheduler s { p };
        co::latch l { 2 };

        s.set_executor(p);

        auto count_down = [&]() -> co::task<> {
            std::this_thread::sleep_for(100ms);
            l.count_down();
            co_return;
        };

        auto wait = [&]() -> co::task<> {
            co_await l.wait();
        };

        s.schedule(wait());
        s.schedule(count_down());
        s.schedule(count_down());

        s.run();
    }

    BOOST_AUTO_TEST_SUITE(latch_)

        BOOST_AUTO_TEST_CASE(count_down_)
        {
            co::basic_scheduler e;
            co::latch l { 1 };

            auto wait = [&]() -> co::task<> {
                co_await l.wait();
            };

            auto count_down = [&]() -> co::task<> {
                l.count_down();
                co_return;
            };

            e.schedule(wait());
            e.schedule(count_down());

            e.run();
        }

        BOOST_AUTO_TEST_CASE(count_down_on_thread_)
        {
            co::basic_scheduler e;
            co::latch l { 3 };

            auto wait = [&]() -> co::task<> {
                co_await l.wait();
            };

            auto count_down = [&]() {
                return std::jthread { [&] {
                    std::this_thread::sleep_for(100ms);
                    l.count_down();
                } };
            };

            e.schedule(wait());

            auto t1 = count_down();
            auto t2 = count_down();
            auto t3 = count_down();

            e.run();
        }

        BOOST_AUTO_TEST_CASE(arrive_and_wait_)
        {
            co::basic_scheduler e;
            co::latch l { 3 };

            auto wait = [&]() -> co::task<> {
                co_await l.wait();
            };

            auto arrive_and_wait = [&]() -> co::task<> {
                co_await l.arrive_and_wait();
            };

            e.schedule(wait());
            e.schedule(arrive_and_wait());
            e.schedule(arrive_and_wait());
            e.schedule(arrive_and_wait());

            e.run();
        }

        BOOST_AUTO_TEST_CASE(arrive_and_wait_on_thread_)
        {
            co::basic_scheduler e;
            co::latch l { 3 };

            auto wait = [&]() -> co::task<> {
                co_await l.wait();
            };

            auto arrive_and_wait = [&]() {
                return std::jthread { [&] {
                    std::this_thread::sleep_for(100ms);
                    e.schedule(l.arrive_and_wait());
                } };
            };

            e.schedule(wait());
            arrive_and_wait();
            arrive_and_wait();
            arrive_and_wait();

            e.run();
        }

    BOOST_AUTO_TEST_SUITE_END() // latch_

BOOST_AUTO_TEST_SUITE_END() // basic_scheduler_

} // namespace testing
