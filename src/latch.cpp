#include <stream9/coroutine/latch.hpp>

#include "namespace.hpp"

#include <thread>

#include <boost/test/unit_test.hpp>

namespace testing {

using namespace std::literals;

BOOST_AUTO_TEST_SUITE(latch_)

    BOOST_AUTO_TEST_CASE(wait_)
    {
        co::latch latch { 1 };

        auto t = latch.wait();
        t.resume();

        BOOST_TEST(!t.done());

        latch.count_down();

        BOOST_TEST(t.done());
    }

    BOOST_AUTO_TEST_CASE(wait_sync_)
    {
        co::latch latch { 1 };

        auto t = latch.wait();

        std::jthread th {
            [&] {
                std::this_thread::sleep_for(100ms);
                latch.count_down();
            }
        };

        t.wait();
    }

    BOOST_AUTO_TEST_CASE(arrive_and_wait_)
    {
        co::latch latch { 2 };

        auto t1 = latch.arrive_and_wait();
        t1.resume();

        BOOST_TEST(!t1.done());

        auto t2 = latch.arrive_and_wait();
        t2.resume();

        BOOST_TEST(t1.done());
        BOOST_TEST(t2.done());
    }

BOOST_AUTO_TEST_SUITE_END() // latch_

} // namespace testing
