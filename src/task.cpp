#include <stream9/coroutine/task.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

namespace co = stream9::coroutine;

BOOST_AUTO_TEST_SUITE(task_)

    BOOST_AUTO_TEST_SUITE(constructor_)

        BOOST_AUTO_TEST_CASE(value_type_)
        {
            auto coro = []() -> co::task<int> {
                co_return 0;
            };

            auto t = coro();

            BOOST_TEST(!t.done());

            auto r = t.get();
            BOOST_TEST(r == 0);
        }

        BOOST_AUTO_TEST_CASE(value_type_2_)
        {
            auto coro = []() -> co::task<int> {
                co_yield 1;
                co_yield 2;
                co_return 3;
            };

            auto t = coro();

            BOOST_TEST(!t.done());

            t.resume();
            BOOST_TEST(t.result() == 1);
            BOOST_TEST(!t.done());

            t.resume();
            BOOST_TEST(t.result() == 2);
            BOOST_TEST(!t.done());

            t.resume();
            BOOST_TEST(t.result() == 3);
            BOOST_TEST(t.done());
        }

        BOOST_AUTO_TEST_CASE(reference_type_)
        {
            int i1 = 100;
            auto coro = [&]() -> co::task<int&> {
                co_return i1;
            };

            auto t = coro();

            BOOST_TEST(!t.done());

            auto r = t.get();
            BOOST_TEST(r == 100);
        }

        BOOST_AUTO_TEST_CASE(void_type_)
        {
            auto coro = []() -> co::task<> {
                co_return;
            };

            auto t = coro();

            BOOST_TEST(!t.done());
        }

        BOOST_AUTO_TEST_CASE(from_awaiter_)
        {
            struct awaiter {
                bool await_ready() { return true; }
                void await_suspend(std::coroutine_handle<>) {}
                void await_resume() {}
            };

            co::task t { awaiter() };
            BOOST_TEST(!t.done());

            t.wait();
            BOOST_TEST(t.done());
        }

    BOOST_AUTO_TEST_SUITE_END() // constructor_

    BOOST_AUTO_TEST_CASE(await_on_task_)
    {
        auto coro1 = []() -> co::task<> {
            co_return;
        };

        auto coro2 = [&]() -> co::task<> {
            co_await coro1();
        };

        auto t = coro2();
        t.resume();
    }

    BOOST_AUTO_TEST_CASE(wait_)
    {
        auto coro = []() -> co::task<> {
            co_return;
        };

        auto t = coro();
        t.wait();
    }

    BOOST_AUTO_TEST_CASE(move_only_type_)
    {
        struct foo { int v = 0; };

        auto coro = [](int v) -> co::task<std::unique_ptr<foo>> {
            co_return std::make_unique<foo>(v);
        };

        auto co = coro(10);
        BOOST_TEST(co.get()->v == 10);

        auto rv = std::move(co).result();
        BOOST_TEST(rv->v == 10);
    }

    BOOST_AUTO_TEST_CASE(co_await_move_only_type_1_)
    {
        struct foo { int v = 0; };

        auto coro1 = [](int v) -> co::task<std::unique_ptr<foo>> {
            co_return std::make_unique<foo>(v);
        };

        auto coro2 = [&](int v) -> co::task<std::unique_ptr<foo>> {
            auto rv = co_await coro1(v);
            co_return std::move(rv);
        };

        auto co = coro2(30);
        BOOST_TEST(co.get()->v == 30);

        auto rv = std::move(co).result();
        BOOST_TEST(rv->v == 30);
    }

    BOOST_AUTO_TEST_CASE(co_await_move_only_type_2_)
    {
        struct foo { int v = 0; };

        auto coro1 = [](int v) -> co::task<std::unique_ptr<foo>> {
            co_return std::make_unique<foo>(v);
        };

        auto coro2 = [&](int v) -> co::task<std::unique_ptr<foo>> {
            co_return co_await coro1(v);
        };

        auto co = coro2(30);
        BOOST_TEST(co.get()->v == 30);

        auto rv = std::move(co).result();
        BOOST_TEST(rv->v == 30);
    }

BOOST_AUTO_TEST_SUITE_END() // task_

} // namespace testing
